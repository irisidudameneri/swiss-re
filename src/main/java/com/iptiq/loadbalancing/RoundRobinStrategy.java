package com.iptiq.loadbalancing;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RoundRobinStrategy implements  SelectionStrategy{
    private int currentProvider = 0;

    /*
     * In case a new provider is added in the Round Robin selection strategy
     * it is taken into account in the next iteration, because the currentProvider
     * index is incremented before.**/

    @Override
    public Provider get(@NotNull HashMap<String, Provider> providersList) {
        int nrOfProviders = providersList.size();
        List<String> keysList = new ArrayList<String>(providersList.keySet());
        Provider provider = providersList.get(keysList.get(currentProvider));
        currentProvider = (currentProvider + 1) % nrOfProviders;
        return provider;
    }
}
