package com.iptiq.loadbalancing;

public class CapacityLimitException extends Exception {
    public CapacityLimitException(String message) {
        super(message);
    }
}
